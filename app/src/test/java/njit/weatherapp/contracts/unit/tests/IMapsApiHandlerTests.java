package njit.weatherapp.contracts.unit.tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import njit.weatherapp.contracts.IMapsApiHandler;
import njit.weatherapp.helpers.GlobalHelper;
import njit.weatherapp.models.LocationResults;

import static org.mockito.Mockito.when;

/**
 * Unit tests for the Maps API handler.
 */
@RunWith(MockitoJUnitRunner.class)
public class IMapsApiHandlerTests
{
    // Mock out Maps API handler.
    @Mock
    private IMapsApiHandler mapsApiHandler;

    // Create composite disposable instance.
    private static CompositeDisposable disposable = new CompositeDisposable();

    // Create a fake address.
    private String address = "MockCity, TestLand";

    private String apiKey = GlobalHelper.MAPS_API_KEY;

    /**
     * Ensures location data can be retrieved.
     */
    @Test
    public void getGeoData_ReturnsLocationData()
    {
        when(mapsApiHandler.getGeoData(address, apiKey)).thenReturn(
                new Single<LocationResults>()
                {
                    @Override
                    protected void subscribeActual(SingleObserver<? super LocationResults> observer)
                    {
                        observer.onSuccess(new LocationResults());
                    }
                }
        );

        Single<LocationResults> result = mapsApiHandler.getGeoData(address, apiKey);
        result.subscribe(new SingleObserver<LocationResults>() {
            @Override
            public void onSubscribe(Disposable d)
            {
                disposable.add(d);
            }

            @Override
            public void onSuccess(LocationResults locationResults)
            {
                assert locationResults instanceof LocationResults;
            }

            @Override
            public void onError(Throwable e)
            {
                assert e instanceof Throwable;
            }
        });
    }


    @Test
    public void getGeoData_ThrowsException()
    {
        when(mapsApiHandler.getGeoData(address, apiKey)).thenReturn(
                new Single<LocationResults>()
                {
                    @Override
                    protected void subscribeActual(SingleObserver<? super LocationResults> observer)
                    {
                        observer.onError(new Exception("Error"));
                    }
                }
        );

        Single<LocationResults> result = mapsApiHandler.getGeoData(address, apiKey);
        result.subscribe(new SingleObserver<LocationResults>() {
            @Override
            public void onSubscribe(Disposable d)
            {
                disposable.add(d);
            }

            @Override
            public void onSuccess(LocationResults locationResults)
            {
                assert locationResults instanceof LocationResults;
            }

            @Override
            public void onError(Throwable e)
            {
                assert e instanceof Throwable;
            }
        });
    }
}
