package njit.weatherapp.contracts.unit.tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import njit.weatherapp.contracts.IWeatherApiHandler;
import njit.weatherapp.helpers.GlobalHelper;
import njit.weatherapp.models.CurrentWeather;

import static org.mockito.Mockito.when;

/**
 * Unit tests for the Weather API handler.
 */
@RunWith(MockitoJUnitRunner.class)
public class IWeatherApiHandlerTests
{
    // Create an IApiHandler mock.
    @Mock
    private static IWeatherApiHandler apiHandlerMock;

    // Create composite disposable instance.
    private static CompositeDisposable disposable = new CompositeDisposable();

    // Parameters for getting current weather:
    private static final double LONGITUDE = 1.2;
    private static final double LATITUDE = 1.3;

    private static final String APPID = GlobalHelper.WEATHER_API_KEY;

    /**
     * Ensures a current weather object is returned on success.
     */
    @Test
    public void getCurrentWeather_ReturnsCurrentWeather()
    {
        when(apiHandlerMock.getCurrentWeather(LONGITUDE, LATITUDE, APPID)).thenReturn(
                new Single<CurrentWeather>()
                {
                    @Override
                    protected void subscribeActual(SingleObserver<? super CurrentWeather> observer)
                    {
                        observer.onSuccess(new CurrentWeather());
                    }
                }
        );

        Single<CurrentWeather> result = apiHandlerMock.getCurrentWeather(LONGITUDE, LATITUDE, APPID);
        result.subscribe(new SingleObserver<CurrentWeather>()
        {
            @Override
            public void onSubscribe(Disposable d)
            {
                disposable.add(d);
            }

            @Override
            public void onSuccess(CurrentWeather currentWeather)
            {
                assert currentWeather instanceof CurrentWeather;
            }

            @Override
            public void onError(Throwable e)
            {
                assert e instanceof Throwable;
            }
        });
    }

    /**
     * Ensures an exception is thrown if the API call fails.
     */
    @Test
    public void getCurrentWeather_ThrowsException()
    {
        when(apiHandlerMock.getCurrentWeather(LONGITUDE, LATITUDE, APPID)).thenReturn(
                new Single<CurrentWeather>()
                {
                    @Override
                    protected void subscribeActual(SingleObserver<? super CurrentWeather> observer)
                    {
                        observer.onError(new Exception("Error"));
                    }
                }
        );

        Single<CurrentWeather> result = apiHandlerMock.getCurrentWeather(LONGITUDE, LATITUDE, APPID);
        result.subscribe(new SingleObserver<CurrentWeather>()
        {
            @Override
            public void onSubscribe(Disposable d)
            {
                disposable.add(d);
            }

            @Override
            public void onSuccess(CurrentWeather currentWeather)
            {
                assert currentWeather instanceof CurrentWeather;
            }

            @Override
            public void onError(Throwable e)
            {
                assert e instanceof Exception;
            }
        });
    }
}

