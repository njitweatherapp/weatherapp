package njit.weatherapp.injector.unit.tests;

import org.junit.Test;

import njit.weatherapp.contracts.IMapsApiHandler;
import njit.weatherapp.contracts.IWeatherApiHandler;
import njit.weatherapp.helpers.ApiHelper;
import njit.weatherapp.helpers.GlobalHelper;
import njit.weatherapp.helpers.RetrofitHelper;
import njit.weatherapp.injection.Injector;
import njit.weatherapp.libraries.ForecastCalculator;
import njit.weatherapp.libraries.MetricConverter;

/**
 * Unit tests for the injector.
 */
public class InjectorTests
{
    /**
     * Ensures that the Retrofit helper class is injected.
     */
    @Test
    public void injectRetrofit_InjectsRetroFit()
    {
        RetrofitHelper result = Injector.injectRetroFit(GlobalHelper.WEATHER_BASE_URL);

        assert result instanceof RetrofitHelper;
    }

    /**
     * Ensures the API helper class is injected.
     */
    @Test
    public void injectApiHelper_InjectsApiHelper()
    {
        ApiHelper result = Injector.injectApiHelper(
                Injector.injectRetroFit(GlobalHelper.WEATHER_BASE_URL)
        );

        assert result instanceof ApiHelper;
    }

    /**
     * Ensures the API handler is injected.
     */
    @Test
    public void injectApiHandler_InjectsWeatherApiHandler()
    {
        IWeatherApiHandler result = Injector.injectWeatherApiHandler(
                Injector.injectApiHelper(
                        Injector.injectRetroFit(GlobalHelper.WEATHER_BASE_URL)
                )
        );

        assert result instanceof IWeatherApiHandler;
    }

    /**
     * Ensures the API Maps handler is injected.
     */
    @Test
    public void injectMapsApiHandler_InjectsMapsApiHandler()
    {
        IMapsApiHandler result = Injector.injectMapsApiHandler(
                Injector.injectApiHelper(
                        Injector.injectRetroFit(GlobalHelper.MAPS_BASE_URL)
                )
        );

        assert result instanceof IMapsApiHandler;
    }


    /**
     * Emsures the metric converter is injected.
     */
    @Test
    public void injectMetricConverter_InjectsMetricConverter()
    {
        MetricConverter result = Injector.injectMetricConverter();
        assert result instanceof MetricConverter;
    }

    @Test
    public void injectForecastCalculator_InjectsForecastCalculator()
    {
        ForecastCalculator result = Injector.injectForecastCalculator(
                Injector.injectMetricConverter()
        );
        assert result instanceof ForecastCalculator;
    }
}
