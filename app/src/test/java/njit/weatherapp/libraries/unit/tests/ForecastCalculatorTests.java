package njit.weatherapp.libraries.unit.tests;

import org.junit.Test;

import java.util.ArrayList;

import njit.weatherapp.libraries.ForecastCalculator;
import njit.weatherapp.libraries.MetricConverter;
import njit.weatherapp.models.WeatherDataList;

/**
 * Unit tests for the forecast calculator.
 */
public class ForecastCalculatorTests
{
    // Create new forecast calculator.
    private ForecastCalculator forecastCalculator = new ForecastCalculator(new MetricConverter());

    // Dummy data for testing.
   private ArrayList<WeatherDataList> weatherDataLists = new ArrayList<WeatherDataList>();
   private WeatherDataList data = new WeatherDataList();

    /**
     * Ensures that the end of the first day can be determined.
     */
    @Test
    public void determineEndOfFistDayForecastInDataSet_ReturnsResults()
    {
        // Setup dummy data.
        data.setDtTxt("2019-04-20 12:00:00");
        weatherDataLists.add(data);

        int result = forecastCalculator.determineEndOfFistDayForecastInDataSet(weatherDataLists);

        assert result == 1;
    }

    /**
     * Ensures a list size of 0 will throw an error.
     */
    @Test
    public void determineEndOfFistDayForecastInDataSet_ThrowsAnIllegalArgumentException()
    {
        try
        {
            int result = forecastCalculator.determineEndOfFistDayForecastInDataSet(weatherDataLists);
        }
        catch (IllegalArgumentException e)
        {
            assert e instanceof IllegalArgumentException;
        }
    }
}
