package njit.weatherapp.libraries.unit.tests;

import org.junit.Test;

import njit.weatherapp.libraries.MetricConverter;

/**
 * Unit tests for the Weather API handler.
 */
public class MetricConverterTests
{
    private MetricConverter metricConverter = new MetricConverter();

    @Test
    public void convertKelvinToFahrenheit_ReturnsResult()
    {
        int result = metricConverter.convertKelvinToFahrenheit(225.1);
        assert result == -54;
    }

    @Test
    public void converthPaToinHg_ReturnsResult()
    {
        double result = metricConverter.converthPaToinHg(29.9, 2);
        assert result == 0.89;
    }

    @Test
    public void convertDegreesToCardinalDirection()
    {
        String result = metricConverter.convertDegreesToCardinalDirection(360.0);
        assert result.equals("N");
    }
}
