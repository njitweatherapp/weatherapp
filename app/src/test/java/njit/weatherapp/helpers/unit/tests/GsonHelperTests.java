package njit.weatherapp.helpers.unit.tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import njit.weatherapp.helpers.GsonHelper;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GsonHelperTests
{
    // Create GsonHelper mock.
    @Mock
    private static GsonHelper gsonHelperMock;

    /**
     * Ensures that the GSON helper provides a GsonConverterFactory instance.
     */
    @Test
    public void provideGsonConverterFactory_ProvidesGsonConverterFactory()
    {
        GsonConverterFactory result = GsonHelper.provideGsonConverterFactory();

        assert result instanceof GsonConverterFactory;
    }

    /**
     * Ensures that if the GSON helper provider has an initialization error it will throw an
     * exception.
     */
    @Test(expected = Exception.class)
    public void provideGsonConverterFactory_ThrowsException()
    {
        when(gsonHelperMock.provideGsonConverterFactory()).thenThrow(Exception.class);
    }
}
