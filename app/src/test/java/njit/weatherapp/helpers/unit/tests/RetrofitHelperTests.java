package njit.weatherapp.helpers.unit.tests;

import org.junit.Test;

import njit.weatherapp.helpers.GlobalHelper;
import njit.weatherapp.helpers.GsonHelper;
import njit.weatherapp.helpers.RetrofitHelper;
import njit.weatherapp.helpers.RxJavaHelper;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitHelperTests
{
    // Get base URL from global helper.
    private static final String BASEURL = GlobalHelper.WEATHER_BASE_URL;

    // Get Gson converter factory instance.
    private static GsonConverterFactory gsonFactory = GsonHelper.provideGsonConverterFactory();

    // Get JavaRx2CallAdapter factory instance.
    private static RxJava2CallAdapterFactory rxJavaFactory = RxJavaHelper.provideRxJava2CallAdapterFactory();

    /**
     * Ensure the Retrofit helper class can provide Retrofit.
     */
    @Test
    public void provideRetrofit_ProvidesRetrofit()
    {
        RetrofitHelper retrofitHelper = new RetrofitHelper(BASEURL, gsonFactory, rxJavaFactory);

        Retrofit result = retrofitHelper.provideRetrofit();

        assert result instanceof Retrofit;
    }

    /**
     * Ensure an exception is thrown when invalid data is provided to the Retrofit helper.
     */
    @Test(expected = Exception.class)
    public void provideRetrofit_ThrowsException()
    {
        RetrofitHelper retrofitHelper = new RetrofitHelper(null, null, null);

        retrofitHelper.provideRetrofit();
    }
}
