package njit.weatherapp.helpers.unit.tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import njit.weatherapp.helpers.RxJavaHelper;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RxJavaHelperTests
{
    // Create a RxJavaHelper mock.
    @Mock
    private static RxJavaHelper rxJavaHelperMock;

    /**
     * Ensures that the RxJavaHelper class provides a RxJava2CallAdapterFactory.
     */
    @Test
    public void provideRxJava2CallAdapterFactory_ProvidesRxJava2CallAdapterFactory()
    {
        RxJava2CallAdapterFactory result = RxJavaHelper.provideRxJava2CallAdapterFactory();
    }

    /**
     * Ensures that the RxJavaHelper class will thrown an exception if it fails to provide
     * the RxJava2CallAdapterFactory
     */
    @Test(expected = Exception.class)
    public void provideRxJava2CallAdapterFactory_ThrowsException()
    {
        when(rxJavaHelperMock.provideRxJava2CallAdapterFactory()).thenThrow(Exception.class);
    }
}
