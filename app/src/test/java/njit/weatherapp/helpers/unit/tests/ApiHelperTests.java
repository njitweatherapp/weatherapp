package njit.weatherapp.helpers.unit.tests;

import org.junit.Test;

import njit.weatherapp.contracts.IWeatherApiHandler;
import njit.weatherapp.helpers.ApiHelper;
import njit.weatherapp.helpers.GlobalHelper;
import njit.weatherapp.injection.Injector;
import retrofit2.Retrofit;

public class ApiHelperTests
{
    // Create a retrofit object.
    private static Retrofit retrofit = Injector.injectRetroFit(GlobalHelper.WEATHER_BASE_URL).provideRetrofit();

    /**
     * Ensure that an IApiHandler instance is returned when providing the API handler.
     */
    @Test
    public void ProvideApiHandler_ProvidesHandler()
    {
        ApiHelper apiHelper = new ApiHelper(retrofit);

        IWeatherApiHandler result = apiHelper.provideWeatherApiHandler();

        assert result instanceof IWeatherApiHandler;
    }

    /**
     * Ensures that an exception is thrown if the retrofit provided is null or invalid.
     */
    @Test(expected = Exception.class)
    public void ProvideApiHandler_ThrowsException()
    {
        ApiHelper apiHelper = new ApiHelper(null);

        apiHelper.provideWeatherApiHandler();
    }
}
