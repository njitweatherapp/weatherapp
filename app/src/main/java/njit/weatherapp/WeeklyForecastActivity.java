package njit.weatherapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import njit.weatherapp.contracts.IForecastCalculator;
import njit.weatherapp.contracts.IWeatherApiHandler;
import njit.weatherapp.helpers.GlobalHelper;
import njit.weatherapp.helpers.RxJavaHelper;
import njit.weatherapp.injection.Injector;
import njit.weatherapp.models.ForecastResults;

public class WeeklyForecastActivity extends AppCompatActivity {

    private IWeatherApiHandler weatherApiHandler;
    private IForecastCalculator forecastCalculator;
    private CompositeDisposable compositeDisposable;

    private double latitude;
    private double longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weekly_forecast);

        // Get coordinates sent over from the Main Activity.
        Intent intent = getIntent();
        double[] coordinates = intent.getDoubleArrayExtra(MainActivity.EXTRA_COORDINATES);
        latitude = coordinates[0];
        longitude = coordinates[1];

        // Inject API handler instance for weather API calls.
        weatherApiHandler = Injector.injectWeatherApiHandler(
                Injector.injectApiHelper(
                        Injector.injectRetroFit(GlobalHelper.WEATHER_BASE_URL)
                )
        );

        // Inject forecast calculator instance.
        forecastCalculator = Injector.injectForecastCalculator(Injector.injectMetricConverter());

        // Provide a composite disposable to handle memory leaks.
        compositeDisposable = RxJavaHelper.provideDisposable();

        // Get forecast.
        loadForecastedWeather(latitude, longitude);
    }

    // Gets the weather data from the API.
    private void loadForecastedWeather(double latitude, double longitude)
    {
        weatherApiHandler.getForecast(latitude, longitude, GlobalHelper.WEATHER_API_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ForecastResults>()
                {
                    @Override
                    public void onSubscribe(Disposable d)
                    {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(ForecastResults forecastResults)
                    {
                        Log.i("loadForecastedWeather", "Received: " + forecastResults.toString());

                        // Load data into GUI.
                        loadForecastedWeatherIntoGUI(forecastResults);
                    }

                    @Override
                    public void onError(Throwable e)
                    {
                        Log.e("loadForecastedWeather", e.toString());
                    }
                });
    }

    private void loadForecastedWeatherIntoGUI(ForecastResults forecastResults)
    {
        // Setup date info.
        Date today = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEE");

        // Load city.
        TextView cityTextView = findViewById(R.id.city);
        cityTextView.setText(forecastResults.getCity().getName());

        // Get current day.
        String day1 = simpleDateFormat.format(today);

        // Determine ending index of current day in the data set.
        int endOfFirstDayIndex = forecastCalculator.determineEndOfFistDayForecastInDataSet(forecastResults.getWeatherDataList());
        if(endOfFirstDayIndex == -1)
        {
            TextView errorMessage = findViewById(R.id.errorMessage);
            String message = "Had trouble loading weather data.";
            errorMessage.setText(message);
        }
        else
        {
            // Load current day.
            TextView day1TextView = findViewById(R.id.day1);
            day1TextView.setText(day1);

            // Load current temp for current day.
            TextView day1CurrentTempTextView = findViewById(R.id.currentWeather1);
            day1CurrentTempTextView.setText(forecastResults.getWeatherDataList().get(endOfFirstDayIndex).getWeather().get(0).getMain());

            // Load mean high for current day.
            TextView day1MeanHighTempTextView = findViewById(R.id.maxTemp1);
            String day1MeanHighTemp = Integer.toString(forecastCalculator.getAverageHighTemperature(forecastResults.getWeatherDataList(), endOfFirstDayIndex)) + " F";
            day1MeanHighTempTextView.setText(day1MeanHighTemp);

            // Load mean rain for current day.
            TextView day1MeanRainTextView = findViewById(R.id.rain1);
            String day1MeanRain = Double.toString(forecastCalculator.getAverageRain(forecastResults.getWeatherDataList(), endOfFirstDayIndex, 2)) + " mm";
            day1MeanRainTextView.setText(day1MeanRain);

            // Load mean pressure for current day.
            TextView day1MeanPressureTextView = findViewById(R.id.pressure1);
            String day1MeanPressure = Double.toString(forecastCalculator.getAveragePressure(forecastResults.getWeatherDataList(), endOfFirstDayIndex)) + " Hg";
            day1MeanPressureTextView.setText(day1MeanPressure);

            // Load mean humidity for current day.
            TextView day1MeanHumidityTextView = findViewById(R.id.humidity1);
            String day1MeanHumidity = Integer.toString(forecastCalculator.getAverageHumidity(forecastResults.getWeatherDataList(), endOfFirstDayIndex)) + " %";
            day1MeanHumidityTextView.setText(day1MeanHumidity);

            // Load mean wind speed for current day.
            TextView day1MeanWindTextView = findViewById(R.id.wind1);
            String day1MeanWind = Double.toString(forecastCalculator.getAverageWind(forecastResults.getWeatherDataList(), 2, endOfFirstDayIndex)) + " " +
                    forecastCalculator.getAverageDegrees(forecastResults.getWeatherDataList(), endOfFirstDayIndex);
            day1MeanWindTextView.setText(day1MeanWind);

            // Get day 2
            calendar.add(Calendar.DATE, 1);

            // Load day 2.
            TextView day2TextView = findViewById(R.id.day2);
            String day2 = simpleDateFormat.format(calendar.getTime());
            day2TextView.setText(day2);

            // Load current temp for day 2.
            TextView day2CurrentTempTextView = findViewById(R.id.currentWeather2);
            day2CurrentTempTextView.setText(forecastResults.getWeatherDataList().get(endOfFirstDayIndex + 7).getWeather().get(0).getMain());

            // Load mean high for day 2.
            TextView day2MeanHighTempTextView = findViewById(R.id.maxTemp2);
            String day2MeanHighTemp = Integer.toString(forecastCalculator.getAverageHighTemperature(2, forecastResults.getWeatherDataList())) + " F";
            day2MeanHighTempTextView.setText(day2MeanHighTemp);

            // Load mean rain for day 2.
            TextView day2MeanRainTextView = findViewById(R.id.rain2);
            String day2MeanRain = Double.toString(forecastCalculator.getAverageRain(2, forecastResults.getWeatherDataList(), 2)) + " mm";
            day2MeanRainTextView.setText(day2MeanRain);

            // Load mean pressure for day 2.
            TextView day2MeanPressureTextView = findViewById(R.id.pressure2);
            String day2MeanPressure = Double.toString(forecastCalculator.getAveragePressure(2, forecastResults.getWeatherDataList())) + " Hg";
            day2MeanPressureTextView.setText(day2MeanPressure);

            // Load humidity for day 2.
            TextView day2MeanHumidityTextView = findViewById(R.id.humidity2);
            String day2MeanHumidity = Integer.toString(forecastCalculator.getAverageHumidity(2, forecastResults.getWeatherDataList())) + " %";
            day2MeanHumidityTextView.setText(day2MeanHumidity);

            // Load mean wind for day 2.
            TextView day2MeanWindTextView = findViewById(R.id.wind2);
            String day2MeanWind = Double.toString(forecastCalculator.getAverageWind(2, forecastResults.getWeatherDataList(), 2)) + " " +
                    forecastCalculator.getAverageDegrees(2, forecastResults.getWeatherDataList());
            day2MeanWindTextView.setText(day2MeanWind);

            // Get day 3.
            calendar.add(Calendar.DATE, 1);

            // Load day 3.
            TextView day3TextView = findViewById(R.id.day3);
            String day3 = simpleDateFormat.format(calendar.getTime());
            day3TextView.setText(day3);

            // Load current temp for day 3.
            TextView day3CurrentTempTextView = findViewById(R.id.currentWeather3);
            day3CurrentTempTextView.setText(forecastResults.getWeatherDataList().get(endOfFirstDayIndex + 15).getWeather().get(0).getMain());

            // Load mean high for day 3.
            TextView day3MeanHighTempTextView = findViewById(R.id.maxTemp3);
            String day3MeanHighTemp = Integer.toString(forecastCalculator.getAverageHighTemperature(3, forecastResults.getWeatherDataList())) + " F";
            day3MeanHighTempTextView.setText(day3MeanHighTemp);

            // Load mean rain for day 3.
            TextView day3MeanRainTextView = findViewById(R.id.rain3);
            String day3MeanRain = Double.toString(forecastCalculator.getAverageRain(3, forecastResults.getWeatherDataList(), 2)) + " mm";
            day3MeanRainTextView.setText(day3MeanRain);

            // Load mean pressure for day 3.
            TextView day3MeanPressureTextView = findViewById(R.id.pressure3);
            String day3MeanPressure = Double.toString(forecastCalculator.getAveragePressure(3, forecastResults.getWeatherDataList())) + " Hg";
            day3MeanPressureTextView.setText(day3MeanPressure);

            // Load humidity for day 3.
            TextView day3MeanHumidityTextView = findViewById(R.id.humidity3);
            String day3MeanHumidity = Integer.toString(forecastCalculator.getAverageHumidity(3, forecastResults.getWeatherDataList())) + " %";
            day3MeanHumidityTextView.setText(day3MeanHumidity);

            // Load mean wind for day 3.
            TextView day3MeanWindTextView = findViewById(R.id.wind3);
            String day3MeanWind = Double.toString(forecastCalculator.getAverageWind(3, forecastResults.getWeatherDataList(), 2)) + " " +
                    forecastCalculator.getAverageDegrees(3, forecastResults.getWeatherDataList());
            day3MeanWindTextView.setText(day3MeanWind);

            // Get day 4.
            calendar.add(Calendar.DATE, 1);

            // Load day 4.
            TextView day4TextView = findViewById(R.id.day4);
            String day4 = simpleDateFormat.format(calendar.getTime());
            day4TextView.setText(day4);

            // Load current temp for day 4.
            TextView day4CurrentTempTextView = findViewById(R.id.currentWeather4);
            day4CurrentTempTextView.setText(forecastResults.getWeatherDataList().get(endOfFirstDayIndex + 23).getWeather().get(0).getMain());

            // Load mean high for day 4.
            TextView day4MeanHighTempTextView = findViewById(R.id.maxTemp4);
            String day4MeanHighTemp = Integer.toString(forecastCalculator.getAverageHighTemperature(4, forecastResults.getWeatherDataList())) + " F";
            day4MeanHighTempTextView.setText(day4MeanHighTemp);

            // Load mean rain for day 4.
            TextView day4MeanRainTextView = findViewById(R.id.rain4);
            String day4MeanRain = Double.toString(forecastCalculator.getAverageRain(4, forecastResults.getWeatherDataList(), 2)) + " mm";
            day4MeanRainTextView.setText(day4MeanRain);

            // Load mean pressure for day 4.
            TextView day4MeanPressureTextView = findViewById(R.id.pressure4);
            String day4MeanPressure = Double.toString(forecastCalculator.getAveragePressure(4, forecastResults.getWeatherDataList())) + " Hg";
            day4MeanPressureTextView.setText(day4MeanPressure);

            // Load humidity for day 4.
            TextView day4MeanHumidityTextView = findViewById(R.id.humidity4);
            String day4MeanHumidity = Integer.toString(forecastCalculator.getAverageHumidity(4, forecastResults.getWeatherDataList())) + " %";
            day4MeanHumidityTextView.setText(day4MeanHumidity);

            // Load mean wind for day 4.
            TextView day4MeanWindTextView = findViewById(R.id.wind4);
            String day4MeanWind = Double.toString(forecastCalculator.getAverageWind(4, forecastResults.getWeatherDataList(), 2)) + " " +
                    forecastCalculator.getAverageDegrees(4, forecastResults.getWeatherDataList());
            day4MeanWindTextView.setText(day4MeanWind);

            // Get day 5.
            calendar.add(Calendar.DATE, 1);

            // Load day 5.
            TextView day5TextView = findViewById(R.id.day5);
            String day5 = simpleDateFormat.format(calendar.getTime());
            day5TextView.setText(day5);

            // Load current temp for day 5.
            TextView day5CurrentTempTextView = findViewById(R.id.currentWeather5);
            day5CurrentTempTextView.setText(forecastResults.getWeatherDataList().get(endOfFirstDayIndex + 31).getWeather().get(0).getMain());

            // Load mean high for day 5.
            TextView day5MeanHighTempTextView = findViewById(R.id.maxTemp5);
            String day5MeanHighTemp = Integer.toString(forecastCalculator.getAverageHighTemperature(5, forecastResults.getWeatherDataList())) + " F";
            day5MeanHighTempTextView.setText(day5MeanHighTemp);

            // Load mean rain for day 5.
            TextView day5MeanRainTextView = findViewById(R.id.rain5);
            String day5MeanRain = Double.toString(forecastCalculator.getAverageRain(5, forecastResults.getWeatherDataList(), 2)) + " mm";
            day5MeanRainTextView.setText(day5MeanRain);

            // Load mean pressure for day 5.
            TextView day5MeanPressureTextView = findViewById(R.id.pressure5);
            String day5MeanPressure = Double.toString(forecastCalculator.getAveragePressure(5, forecastResults.getWeatherDataList())) + " Hg";
            day5MeanPressureTextView.setText(day5MeanPressure);

            // Load humidity for day 5.
            TextView day5MeanHumidityTextView = findViewById(R.id.humidity5);
            String day5MeanHumidity = Integer.toString(forecastCalculator.getAverageHumidity(5, forecastResults.getWeatherDataList())) + " %";
            day5MeanHumidityTextView.setText(day5MeanHumidity);

            // Load mean wind for day 5.
            TextView day5MeanWindTextView = findViewById(R.id.wind5);
            String day5MeanWind = Double.toString(forecastCalculator.getAverageWind(5, forecastResults.getWeatherDataList(), 2)) + " " +
                    forecastCalculator.getAverageDegrees(5, forecastResults.getWeatherDataList());
            day5MeanWindTextView.setText(day5MeanWind);
        }
    }
}
