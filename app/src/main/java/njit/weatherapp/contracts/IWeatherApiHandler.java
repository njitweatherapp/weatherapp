package njit.weatherapp.contracts;

import io.reactivex.Single;
import njit.weatherapp.models.CurrentWeather;
import njit.weatherapp.models.ForecastResults;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Makes calls to the weather API.
 */
public interface IWeatherApiHandler
{
    /**
     * Gets the current weather for a given set of coordinates.
     *
     * @param latitude the latitude coordinate for the current location of the device.
     * @param longitude the longitude coordinate for the current location of the device.
     * @param appid the app id required to access the API.
     * @return The current weather.
     */
    @GET("weather")
    Single<CurrentWeather> getCurrentWeather(@Query("lat") double latitude,
                                             @Query("lon") double longitude,
                                             @Query("appid") String appid);

    /**
     * Gets the forecast for a given set of coordinates.
     *
     * @param latitude the latitude coordinate for the current location of the device.
     * @param longitude the longitude coordinate for the current location of the device.
     * @param appid the app id required to access the API.
     * @return The forecasted weather.
     */
    @GET("forecast")
    Single<ForecastResults> getForecast(@Query("lat") double latitude,
                                             @Query("lon") double longitude,
                                             @Query("appid") String appid);
}
