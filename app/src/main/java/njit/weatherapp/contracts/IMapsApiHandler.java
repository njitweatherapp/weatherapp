package njit.weatherapp.contracts;

import io.reactivex.Single;
import njit.weatherapp.models.LocationResults;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Makes calls to the Google Maps API.
 */
public interface IMapsApiHandler
{
    /**
     * Gets the JSON geolocation data for a given address.
     *
     * @param address The address of a location.
     * @param apiKey The Google Maps API key for authentication.
     * @return The location results.
     */
    @GET("/maps/api/geocode/json")
    Single<LocationResults>getGeoData(@Query("address") String address, @Query("key") String apiKey);
}
