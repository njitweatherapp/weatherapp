package njit.weatherapp.contracts;

/**
 * Converts values in the metric system.
 */
public interface IMetricConverter
{
    double BASE_KELVIN_TO_CELSIUS = 273.15;
    double BASE_HPA_TO_inHG = 0.02953;
    String CARDINAL_DIRECTIONS[] = { "N", "NE", "E", "SE", "S", "SW", "W", "NW" };

    /**
     * Converts kelvin units to fahrenheit units.
     *
     * @param kelvin The current measurement in kelvin.
     * @return The units in fahrenheit.
     */
    int convertKelvinToFahrenheit(Double kelvin);

    /**
     * Converts hPa units to inHg units.
     *
     * @param hPa The current measurement in hPa.
     * @param decimalPlaces The number of decimal places to round to.
     * @return The units in inHg.
     */
    double converthPaToinHg(Double hPa, int decimalPlaces);

    /**
     * Converts degrees into cardinal direction.
     *
     * @param degrees The directional measurement in degrees.
     * @return A cardinal direction.
     */
    String convertDegreesToCardinalDirection(Double degrees);
}
