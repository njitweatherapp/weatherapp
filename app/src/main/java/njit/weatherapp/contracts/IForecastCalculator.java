package njit.weatherapp.contracts;

import java.util.List;

import njit.weatherapp.models.WeatherDataList;

/**
 * Helps calculate the forecast.
 */
public interface IForecastCalculator
{
    /**
     * Determines where the data set ends for the first day forecast in the set.
     *
     * @param forecastData The forecast data.
     * @return The index of where the set ends for the first day of the forecast. Or -1 if there was
     * an issue parsing the date.
     */
    int determineEndOfFistDayForecastInDataSet(List<WeatherDataList> forecastData);

    /**
     * Gets the mean high temperature for the given day.
     *
     * @param day The number of day out of the week (0 exclusive).
     * @param forecastData The forecast data.
     * @exception IllegalArgumentException thrown if the passed day value is 0.
     * @return The mean high temperature of the given day.
     */
    int getAverageHighTemperature(int day, List<WeatherDataList> forecastData);

    /**
     * Gets the mean high temperature for the first day.
     *
     * @param forecastData The forecast data.
     * @param endOfFirstDayIndex The index of the end of the first day subset.
     * @return The mean high temperature of the first day.
     */
    int getAverageHighTemperature(List<WeatherDataList> forecastData, int endOfFirstDayIndex);

    /**
     * Gets the mean rain probability for the given day.
     *
     * @param day The number of day out of the week (0 exclusive).
     * @param forecastData The forecast data.
     * @param decimalPlaces The number of decimal places to round to.
     * @exception IllegalArgumentException thrown if the passed day value is 0.
     * @return The mean rain probability of the given day.
     */
    double getAverageRain(int day, List<WeatherDataList> forecastData, int decimalPlaces);

    /**
     * Gets the mean rain probability for the first day.
     *
     * @param forecastData The forecast data.
     * @param endOfFirstDayIndex The index of the end of the first day subset.
     * @param decimalPlaces The number of decimal places to round to.
     * @return The mean rain probability of the first day.
     */
    double getAverageRain(List<WeatherDataList> forecastData, int endOfFirstDayIndex, int decimalPlaces);

    /**
     * Gets the mean pressure for the given day.
     *
     * @param day The number of day out of the week (0 exclusive).
     * @param forecastData The forecast data.
     * @exception IllegalArgumentException thrown if the passed day value is 0.
     * @return The mean pressure of the given day.
     */
    double getAveragePressure(int day, List<WeatherDataList> forecastData);

    /**
     * Gets the mean pressure for the first day.
     *
     * @param forecastData The forecast data.
     * @param endOfFirstDayIndex The index of the end of the first day subset.
     * @return The mean pressure of the first day.
     */
    double getAveragePressure(List<WeatherDataList> forecastData, int endOfFirstDayIndex);

    /**
     * Gets the mean humidity for the given day.
     *
     * @param day The number of day out of the week (0 exclusive).
     * @param forecastData The forecast data.
     * @exception IllegalArgumentException thrown if the passed day value is 0.
     * @return The mean humidity of the given day.
     */
    int getAverageHumidity(int day, List<WeatherDataList> forecastData);

    /**
     * Gets the mean humidity for the first day.
     *
     * @param forecastData The forecast data.
     * @param endOfFirstDayIndex The index of the end of the first day subset.
     * @return The mean humidity of the first day.
     */
    int getAverageHumidity(List<WeatherDataList> forecastData, int endOfFirstDayIndex);

    /**
     * Gets the mean wind for the given day.
     *
     * @param day The number of day out of the week (0 exclusive).
     * @param forecastData The forecast data.
     * @param decimalPlaces The number of decimals to round to.
     * @exception IllegalArgumentException thrown if the passed day value is 0.
     * @return The mean wind of the given day.
     */
    double getAverageWind(int day, List<WeatherDataList> forecastData, int decimalPlaces);

    /**
     * Gets the mean wind for the first day.
     *
     * @param forecastData The forecast data.
     * @param decimalPlaces The number of decimals to round to.
     * @param endOfFirstDayIndex The index of the end of the first day subset.
     * @return The mean wind of the first day.
     */
    double getAverageWind(List<WeatherDataList> forecastData, int decimalPlaces, int endOfFirstDayIndex);

    /**
     * Gets the mean wind direction for the given day.
     *
     * @param day The number of day out of the week (0 exclusive).
     * @param forecastData The forecast data.
     * @exception IllegalArgumentException thrown if the passed day value is 0.
     * @return The mean wind direction of the first day.
     */
    String getAverageDegrees(int day, List<WeatherDataList> forecastData);

    /**
     * Gets the mean wind direction for the given day.
     *.
     * @param forecastData The forecast data.
     * @param endOfFirstDayIndex The index of the end of the first day subset.
     * @return The mean wind direction of the first day.
     */
    String getAverageDegrees(List<WeatherDataList> forecastData, int endOfFirstDayIndex);
}
