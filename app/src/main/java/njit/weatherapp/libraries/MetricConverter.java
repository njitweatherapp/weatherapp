package njit.weatherapp.libraries;

import java.math.RoundingMode;
import java.text.DecimalFormat;

import njit.weatherapp.contracts.IMetricConverter;

/**
 * Converts values in the metric system.
 */
public class MetricConverter implements IMetricConverter
{
    /**
     * Converts kelvin units to fahrenheit units.
     *
     * @param kelvin The current measurement in kelvin.
     * @return The units in fahrenheit.
     */
    public int convertKelvinToFahrenheit(Double kelvin)
    {
        if(kelvin == null)
        {
            kelvin = 0.0;
        }
        return (int)(kelvin - BASE_KELVIN_TO_CELSIUS) * 9/5 + 32;
    }

    /**
     * Converts hPa units to inHg units.
     *
     * @param hPa The current measurement in hPa.
     * @param decimalPlaces The number of decimal places to round to.
     * @return The units in inHg.
     */
    public double converthPaToinHg(Double hPa, int decimalPlaces)
    {
        if(hPa != null)
        {
            // Setup formatter to round to x decimal places.
            String dFromat = "";
            for (int i = 0; i < decimalPlaces; i++)
            {
                dFromat += "#";
            }

            DecimalFormat decimalFormat = new DecimalFormat("#." + dFromat);
            decimalFormat.setRoundingMode(RoundingMode.CEILING); // Round up.

            return Double.parseDouble(decimalFormat.format(hPa * BASE_HPA_TO_inHG));
        }
        else
        {
            return 0;
        }
    }

    /**
     * Converts degrees into cardinal direction.
     *
     * @param degrees The directional measurement in degrees.
     * @return A cardinal direction.
     */
    public String convertDegreesToCardinalDirection(Double degrees)
    {
        if(degrees == null)
        {
            degrees = 0.0;
        }

        // Computation source: https://stackoverflow.com/a/2131294
        return CARDINAL_DIRECTIONS[((int)Math.round(degrees % 360) / 45) % 8];
    }
}
