package njit.weatherapp.libraries;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import njit.weatherapp.contracts.IForecastCalculator;
import njit.weatherapp.contracts.IMetricConverter;
import njit.weatherapp.models.WeatherDataList;

/**
 * Helps calculate the forecast.
 */
public class ForecastCalculator implements IForecastCalculator
{
    private IMetricConverter metricConverter;

    /**
     * Creates a new forecast calculator.
     *
     * @param metricConverter Helps convert metric values.
     */
    public ForecastCalculator(IMetricConverter metricConverter)
    {
        this.metricConverter = metricConverter;
    }

    /**
     * Determines where the data set ends for the first day forecast in the set.
     *
     * @param forecastData The forecast data.
     * @return The index of where the set ends for the first day of the forecast. Or -1 if there was
     * an issue parsing the date.
     */
    public int determineEndOfFistDayForecastInDataSet(List<WeatherDataList> forecastData)
    {
        if(forecastData.size() == 0)
        {
            throw new IllegalArgumentException("The size of the data set is less than 1.");
        }

        // Get the date for the first data point in the forecast.
        String dateOfFirstDataPoint = forecastData.get(0).getDtTxt();
        try
        {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date today = simpleDateFormat.parse(dateOfFirstDataPoint);

            int index = 0;
            for(WeatherDataList dataPoint : forecastData)
            {
                Date dateOfThisDataPoint = simpleDateFormat.parse(dataPoint.getDtTxt());
                if(today.getDay() < dateOfThisDataPoint.getDay())
                {
                    return index;
                }
                else
                {
                    index++;
                }
            }

            return index;
        }
        catch (ParseException e)
        {
            return -1;
        }
    }

    /**
     * Gets the mean high temperature for the given day.
     *
     * @param day The number of day out of the week (0 exclusive).
     * @exception IllegalArgumentException thrown if the passed day value is 0.
     * @return The mean high temperature of the given day.
     */
    public int getAverageHighTemperature(int day, List<WeatherDataList> forecastData)
    {
        if(day < 1)
        {
            throw new IllegalArgumentException("Day cannot be less than 1; value passed: " + day);
        }

        int startIndex = (day * 8) - 8;
        int endIndex = (day * 8) - 1;

        int summationOfHigh = 0;
        for(int i = startIndex; i < endIndex; i++)
        {
            Double high = forecastData.get(i).getMain().getTempMax();
            if(high == null)
            {
                summationOfHigh += 0;
            }
            else
            {
                summationOfHigh += metricConverter.convertKelvinToFahrenheit(high);
            }
        }

        return summationOfHigh / 8;
    }

    /**
     * Gets the mean high temperature for the first day.
     *
     * @param forecastData The forecast data.
     * @param endOfFirstDayIndex The index of the end of the first day subset.
     * @return The mean high temperature of the first day.
     */
    public int getAverageHighTemperature(List<WeatherDataList> forecastData, int endOfFirstDayIndex)
    {
        int summationOfHigh = 0;
        for(int i = 0; i <= endOfFirstDayIndex; i++)
        {
            Double high = forecastData.get(i).getMain().getTempMax();
            if(high == null)
            {
                summationOfHigh += 0;
            }
            else
            {
                summationOfHigh += metricConverter.convertKelvinToFahrenheit(high);
            }
        }

        return (summationOfHigh / (endOfFirstDayIndex + 1));
    }

    /**
     * Gets the mean rain probability for the given day.
     *
     * @param day The number of day out of the week (0 exclusive).
     * @param forecastData The forecast data.
     * @param decimalPlaces The number of decimal places to round to.
     * @exception IllegalArgumentException thrown if the passed day value is 0.
     * @return The mean rain probability of the given day.
     */
    public double getAverageRain(int day, List<WeatherDataList> forecastData, int decimalPlaces)
    {
        if(day < 1)
        {
            throw new IllegalArgumentException("Day cannot be less than 1; value passed: " + day);
        }

        int startIndex = (day * 8) - 8;
        int endIndex = (day * 8) - 1;

        double summationOfRain = 0;
        for(int i = startIndex; i < endIndex; i++)
        {
            Double rain;
            try
            {
                rain = forecastData.get(i).getRain().getRainVolume();
            }
            catch(Exception e)
            {
                rain = 0.0;
            }

            if(rain == null)
            {
                summationOfRain += 0.0;
            }
            else
            {
                summationOfRain += rain;
            }
        }

        // Setup formatter to round to x decimal places.
        String dFromat = "";
        for (int i = 0; i < decimalPlaces; i++)
        {
            dFromat += "#";
        }

        DecimalFormat decimalFormat = new DecimalFormat("#." + dFromat);
        decimalFormat.setRoundingMode(RoundingMode.CEILING); // Round up.

        return Double.parseDouble(decimalFormat.format(summationOfRain / 8));
    }

    /**
     * Gets the mean rain probability for the first day.
     *
     * @param forecastData The forecast data.
     * @param endOfFirstDayIndex The index of the end of the first day subset.
     * @param decimalPlaces The number of decimal places to round to.
     * @return The mean rain probability of the first day.
     */
    public double getAverageRain(List<WeatherDataList> forecastData, int endOfFirstDayIndex, int decimalPlaces)
    {
        Double summationOfRain = 0.0;
        for(int i = 0; i <= endOfFirstDayIndex; i++)
        {
            Double rain;
            try
            {
                rain = forecastData.get(i).getRain().getRainVolume();
            }
            catch(Exception e)
            {
                rain = 0.0;
            }

            if(rain == null)
            {
                summationOfRain += 0.0;
            }
            else
            {
                summationOfRain += rain;
            }
        }

        // Setup formatter to round to x decimal places.
        String dFromat = "";
        for (int i = 0; i < decimalPlaces; i++)
        {
            dFromat += "#";
        }

        DecimalFormat decimalFormat = new DecimalFormat("#." + dFromat);
        decimalFormat.setRoundingMode(RoundingMode.CEILING); // Round up.

        return Double.parseDouble(decimalFormat.format(summationOfRain / 8));
    }

    /**
     * Gets the mean pressure for the given day.
     *
     * @param day The number of day out of the week (0 exclusive).
     * @param forecastData The forecast data.
     * @exception IllegalArgumentException thrown if the passed day value is 0.
     * @return The mean pressure of the given day.
     */
    public double getAveragePressure(int day, List<WeatherDataList> forecastData)
    {
        if(day < 1)
        {
            throw new IllegalArgumentException("Day cannot be less than 1; value passed: " + day);
        }

        int startIndex = (day * 8) - 8;
        int endIndex = (day * 8) - 1;

        double summationOfPressure = 0.0;
        for(int i = startIndex; i < endIndex; i++)
        {
            Double pressure = forecastData.get(i).getMain().getPressure();
            if(pressure == null)
            {
                summationOfPressure += 0.0;
            }
            else
            {
                summationOfPressure += pressure;
            }
        }

        return metricConverter.converthPaToinHg(summationOfPressure / 8, 2);
    }

    /**
     * Gets the mean pressure for the first day.
     *
     * @param forecastData The forecast data.
     * @param endOfFirstDayIndex The index of the end of the first day subset.
     * @return The mean pressure of the first day.
     */
    public double getAveragePressure(List<WeatherDataList> forecastData, int endOfFirstDayIndex)
    {
        double summationOfPressure = 0.0;
        for(int i = 0; i <= endOfFirstDayIndex; i++)
        {
            Double pressure = forecastData.get(i).getMain().getPressure();
            if(pressure == null)
            {
                summationOfPressure += 0.0;
            }
            else
            {
                summationOfPressure += pressure;
            }
        }

        return metricConverter.converthPaToinHg((summationOfPressure / (endOfFirstDayIndex + 1 )), 2);
    }

    /**
     * Gets the mean humidity for the given day.
     *
     * @param day The number of day out of the week (0 exclusive).
     * @param forecastData The forecast data.
     * @exception IllegalArgumentException thrown if the passed day value is 0.
     * @return The mean humidity of the given day.
     */
    public int getAverageHumidity(int day, List<WeatherDataList> forecastData)
    {
        if(day < 1)
        {
            throw new IllegalArgumentException("Day cannot be less than 1; value passed: " + day);
        }

        int startIndex = (day * 8) - 8;
        int endIndex = (day * 8) - 1;

        int summationOfhumidity = 0;
        for(int i = startIndex; i < endIndex; i++)
        {
            Integer humidity = forecastData.get(i).getMain().getHumidity();
            if(humidity == null)
            {
                summationOfhumidity += 0.0;
            }
            else
            {
                summationOfhumidity += humidity;
            }
        }

        return summationOfhumidity / 8;
    }

    /**
     * Gets the mean humidity for the first day.
     *
     * @param forecastData The forecast data.
     * @param endOfFirstDayIndex The index of the end of the first day subset.
     * @return The mean humidity of the first day.
     */
     public int getAverageHumidity(List<WeatherDataList> forecastData, int endOfFirstDayIndex)
     {
         int summationOfhumidity = 0;
         for(int i = 0; i <= endOfFirstDayIndex; i++)
         {
             Integer humidity = forecastData.get(i).getMain().getHumidity();
             if(humidity == null)
             {
                 summationOfhumidity += 0.0;
             }
             else
             {
                 summationOfhumidity += humidity;
             }
         }

         return (summationOfhumidity / (endOfFirstDayIndex + 1));
     }

    /**
     * Gets the mean wind for the given day.
     *
     * @param day The number of day out of the week (0 exclusive).
     * @param forecastData The forecast data.
     * @exception IllegalArgumentException thrown if the passed day value is 0.
     * @return The mean wind of the given day.
     */
    public double getAverageWind(int day, List<WeatherDataList> forecastData, int decimalPlaces)
    {
        if(day < 1)
        {
            throw new IllegalArgumentException("Day cannot be less than 1; value passed: " + day);
        }

        int startIndex = (day * 8) - 8;
        int endIndex = (day * 8) - 1;

        double summationOfWind = 0.0;
        for(int i = startIndex; i < endIndex; i++)
        {
            Double wind = forecastData.get(i).getWind().getSpeed();
            if(wind == null)
            {
                summationOfWind += 0.0;
            }
            else
            {
                summationOfWind += wind;
            }
        }

        // Setup formatter to round to x decimal places.
        String dFromat = "";
        for (int i = 0; i < decimalPlaces; i++)
        {
            dFromat += "#";
        }

        DecimalFormat decimalFormat = new DecimalFormat("#." + dFromat);
        decimalFormat.setRoundingMode(RoundingMode.CEILING); // Round up.

        return Double.parseDouble(decimalFormat.format(summationOfWind / 8));
    }

    /**
     * Gets the mean wind for the first day.
     *
     * @param forecastData The forecast data.
     * @param decimalPlaces The number of decimals to round to.
     * @param endOfFirstDayIndex The index of the end of the first day subset.
     * @return The mean wind of the first day.
     */
    public double getAverageWind(List<WeatherDataList> forecastData, int decimalPlaces, int endOfFirstDayIndex)
    {
        double summationOfWind = 0.0;
        for(int i = 0; i <= endOfFirstDayIndex; i++)
        {
            Double wind = forecastData.get(i).getWind().getSpeed();
            if(wind == null)
            {
                summationOfWind += 0.0;
            }
            else
            {
                summationOfWind += wind;
            }
        }

        // Setup formatter to round to x decimal places.
        String dFromat = "";
        for (int i = 0; i < decimalPlaces; i++)
        {
            dFromat += "#";
        }

        DecimalFormat decimalFormat = new DecimalFormat("#." + dFromat);
        decimalFormat.setRoundingMode(RoundingMode.CEILING); // Round up.

        return Double.parseDouble(decimalFormat.format((summationOfWind / (endOfFirstDayIndex + 1))));
    }

    /**
     * Gets the mean wind direction for the given day.
     *
     * @param day The number of day out of the week (0 exclusive).
     * @param forecastData The forecast data.
     * @exception IllegalArgumentException thrown if the passed day value is 0.
     * @return The mean wind direction of the given day.
     */
    public String getAverageDegrees(int day, List<WeatherDataList> forecastData)
    {
        if(day < 1)
        {
            throw new IllegalArgumentException("Day cannot be less than 1; value passed: " + day);
        }

        int startIndex = (day * 8) - 8;
        int endIndex = (day * 8) - 1;

        double summationOfDegrees = 0.0;
        for(int i = startIndex; i < endIndex; i++)
        {
            Double degrees = forecastData.get(i).getWind().getDeg();
            if(degrees == null)
            {
                summationOfDegrees += 0.0;
            }
            else
            {
                summationOfDegrees += degrees;
            }
        }

        return metricConverter.convertDegreesToCardinalDirection(summationOfDegrees / 8);
    }

    /**
     * Gets the mean wind direction for the given day.
     *.
     * @param forecastData The forecast data.
     * @param endOfFirstDayIndex The index of the end of the first day subset.
     * @return The mean wind direction of the first day.
     */
    public String getAverageDegrees(List<WeatherDataList> forecastData, int endOfFirstDayIndex)
    {
        double summationOfDegrees = 0.0;
        for(int i = 0; i <= endOfFirstDayIndex; i++)
        {
            Double degrees = forecastData.get(i).getWind().getDeg();
            if(degrees == null)
            {
                summationOfDegrees += 0.0;
            }
            else
            {
                summationOfDegrees += degrees;
            }
        }

        return metricConverter.convertDegreesToCardinalDirection((summationOfDegrees / (endOfFirstDayIndex + 1)));
    }
}
