package njit.weatherapp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import njit.weatherapp.contracts.IMapsApiHandler;
import njit.weatherapp.contracts.IWeatherApiHandler;
import njit.weatherapp.helpers.GlobalHelper;
import njit.weatherapp.helpers.RxJavaHelper;
import njit.weatherapp.injection.Injector;
import njit.weatherapp.libraries.MetricConverter;
import njit.weatherapp.models.CurrentWeather;
import njit.weatherapp.models.LocationResults;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_COORDINATES = "njit.weatherapp.mainactivity.COORDINATES";

    private IWeatherApiHandler weatherApiHandler;
    private IMapsApiHandler mapsApiHandler;
    private MetricConverter metricConverter;

    private CompositeDisposable compositeDisposable;

    private Location currentLocation;
    private LocationListener locationListener;
    private LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        locationListener = initLocation();
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        // Check for permissions and if we don't have them have the UI request access.
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this, GlobalHelper.permissions, 1137);
        }

        // Get location updates.
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, GlobalHelper.UPDATE_INTERVAL_IN_MILLISECONDS,
                GlobalHelper.REQUEST_CHECK_SETTINGS, locationListener);

        // Inject API handler instance for weather API calls.
        weatherApiHandler = Injector.injectWeatherApiHandler(
                Injector.injectApiHelper(
                        Injector.injectRetroFit(GlobalHelper.WEATHER_BASE_URL)
                )
        );

        // Inject Maps API handler instance for Maps API calls.
        mapsApiHandler = Injector.injectMapsApiHandler(
                Injector.injectApiHelper(
                        Injector.injectRetroFit(GlobalHelper.MAPS_BASE_URL)
                )
        );

        // Inject metric helper instance.
        metricConverter = Injector.injectMetricConverter();

        // Provide a composite disposable to handle memory leaks.
        compositeDisposable = RxJavaHelper.provideDisposable();
    }

    // Initializes location.
    private LocationListener initLocation()
    {
       return new LocationListener()
       {
           @Override
           public void onLocationChanged(final Location location)
           {
               currentLocation = location;
               loadCurrentWeather();
           }

           @Override
           public void onStatusChanged(String provider, int status, Bundle extras) {
                // Do nothing.
           }

           @Override
           public void onProviderEnabled(String provider) {
               // Do nothing.
           }

           @Override
           public void onProviderDisabled(String provider) {
               // Do nothing.
           }
       };
    }

    // Gets the weather data from the API.
    private void loadCurrentWeather()
    {
        if(currentLocation != null)
        {
            weatherApiHandler.getCurrentWeather(currentLocation.getLatitude(),
                    currentLocation.getLongitude(), GlobalHelper.WEATHER_API_KEY)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<CurrentWeather>() {
                        @Override
                        public void onSubscribe(Disposable d)
                        {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(CurrentWeather currentWeather)
                        {
                            Log.d("loadCurrentWeather", currentWeather.getMain().getTemp().toString());
                            loadWeatherIntoGUI(currentWeather);
                        }

                        @Override
                        public void onError(Throwable e)
                        {
                            Log.e("loadCurrentWeather", e.toString());
                        }
                    });
        }
    }

    /**
     * Destroys and removes all disposables.
     */
    @Override
    protected void onDestroy()
    {
        if (!compositeDisposable.isDisposed())
        {
            compositeDisposable.dispose();
        }
        super.onDestroy();
    }

    // Loads the weather data into the GUI.
    private void loadWeatherIntoGUI(CurrentWeather currentWeather)
    {
        // Load main weather description.
        TextView todaysWeatherTextView = findViewById(R.id.todaysWeather);
        String weatherDescription = "Today's Weather Is " + currentWeather.getWeather().get(0).getMain();
        todaysWeatherTextView.setText(weatherDescription);

        // Load city name.
        TextView cityTextView = findViewById(R.id.city);
        cityTextView.setText(currentWeather.getName());

        // Load current temp.
        TextView currentTempTextView = findViewById(R.id.currentTemp);
        String currentTemp = Integer.toString(metricConverter.convertKelvinToFahrenheit(currentWeather.getMain().getTemp())) + " F";
        currentTempTextView.setText(currentTemp);

        // Load high temp.
        TextView highTempTextView = findViewById(R.id.maxTemp1);
        String maxTemp = Integer.toString(metricConverter.convertKelvinToFahrenheit(currentWeather.getMain().getTempMax())) + " F";
        highTempTextView.setText(maxTemp);

        // Load low temp.
        TextView lowTempTextView = findViewById(R.id.rain1);
        String lowTemp = Integer.toString(metricConverter.convertKelvinToFahrenheit(currentWeather.getMain().getTempMin())) + " F";
        lowTempTextView.setText(lowTemp);

        // Load sunrise time.
        DateFormat dateFormat = new SimpleDateFormat("h:mm a");

        TextView sunriseTextView = findViewById(R.id.sunrise);
        Date sunrise = new Date((long)currentWeather.getSys().getSunrise() * 1000);
        sunriseTextView.setText(dateFormat.format(sunrise));

        // Load sunset time.
        TextView sunsetTextView = findViewById(R.id.sunset);
        Date sunset = new Date((long)currentWeather.getSys().getSunset() * 1000);
        sunsetTextView.setText(dateFormat.format(sunset));

        // Load pressure.
        TextView pressureTextView = findViewById(R.id.pressure1);
        String pressure = Double.toString(metricConverter.converthPaToinHg(currentWeather.getMain().getPressure(), 2)) + " inHg";
        pressureTextView.setText(pressure);

        // Load humidity.
        TextView humidityTextView = findViewById(R.id.humidity);
        String humidity = currentWeather.getMain().getHumidity().toString() + "%";
        humidityTextView.setText(humidity);

        // Load wind speed.
        TextView windTextview = findViewById(R.id.wind1);
        String wind = Double.toString(currentWeather.getWind().getSpeed()) + " ";
        String direction = metricConverter.convertDegreesToCardinalDirection(currentWeather.getWind().getDeg());
        windTextview.setText(wind + direction);
    }

    // Gets geo data based on address typed.
    public void getGeoData(View view)
    {
        // Get address in search box.
        EditText editText = findViewById(R.id.weatherSearch);
        final String address = editText.getText().toString();

        if(!address.isEmpty() && address != null)
        {
            mapsApiHandler.getGeoData(address, GlobalHelper.MAPS_API_KEY)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<LocationResults>() {
                        @Override
                        public void onSubscribe(Disposable d)
                        {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(LocationResults locationResults)
                        {
                            Log.i("getGeoData", "Retrieved data: " + locationResults.toString());
                            if(locationResults.getResults().size() == 0)
                            {
                                onError(new Exception());
                            }
                            else
                            {
                                sendGeoDataToForecastActivity(locationResults.getResults().get(0).getGeometry().getLocation());
                            }
                        }

                        @Override
                        public void onError(Throwable e)
                        {
                            Log.e("getGeoData", e.toString(), e);

                            TextView errorMessage = findViewById(R.id.errorMessage);
                            String message = "An error occurred trying to get the forecast for " + address;
                            errorMessage.setText(message);
                        }
                    });
        }
    }

    // Sends the geo data to the Weekly forecast activity.
    private void sendGeoDataToForecastActivity(njit.weatherapp.models.Location location)
    {
        // Load latitude and longitude coordinates into an array.
        double[] coordinates = { location.getLatitude(), location.getLongitude()};

        Intent intent = new Intent(this, WeeklyForecastActivity.class);
        intent.putExtra(EXTRA_COORDINATES, coordinates);
        startActivity(intent);
    }
}
