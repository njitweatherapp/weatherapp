package njit.weatherapp.helpers;

import njit.weatherapp.contracts.IMapsApiHandler;
import njit.weatherapp.contracts.IWeatherApiHandler;
import retrofit2.Retrofit;

/**
 * Helps inject retrofit for the API handlers.
 */
public class ApiHelper
{
    /**
     * Retrofit caller for making API calls.
     */
    public Retrofit retrofit;

    /**
     * Creates a new API Helper.
     *
     * @param retrofit The retrofit instance to bootstrap the API handlers.
     */
    public ApiHelper(Retrofit retrofit)
    {
        this.retrofit = retrofit;
    }

    /**
     * Provides a fully retrofitted Weather API handler.
     *
     * @return A new retrofit instance of type IWeatherAPIHandler.
     */
    public IWeatherApiHandler provideWeatherApiHandler()
    {
        return retrofit.create(IWeatherApiHandler.class);
    }

    /**
     * Provides a fully retrofitted Maps API handler.
     *
     * @return A new retrofitted instance of type IMapsApiHandler.
     */
    public IMapsApiHandler provideMapsApiHandler()
    {
        return retrofit.create(IMapsApiHandler.class);
    }
}
