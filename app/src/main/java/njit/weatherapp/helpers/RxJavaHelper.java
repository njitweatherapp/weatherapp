package njit.weatherapp.helpers;

import io.reactivex.disposables.CompositeDisposable;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

/**
 * Helps inject RxJava into other services.
 */
public class RxJavaHelper
{
    /**
     * Provides a new RxJava2 call adapter factory.
     *
     * @return a new RxJava2 call adapter factory.
     */
    public static RxJava2CallAdapterFactory provideRxJava2CallAdapterFactory()
    {
        return RxJava2CallAdapterFactory.create();
    }

    /**
     * Provides a disposable for closing open connections and
     * preventing memory leaks.
     *
     * @return a new composite disposable.
     */
    public static CompositeDisposable provideDisposable()
    {
        return new CompositeDisposable();
    }
}
