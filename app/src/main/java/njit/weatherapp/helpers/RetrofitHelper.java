package njit.weatherapp.helpers;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Helps inject retro fit into other services.
 */
public class RetrofitHelper
{
    /**
     * Base url to be used when making API calls.
     */
    public static String baseUrl;

    /**
     * GSON converter factory to be used to provide
     * GSON serialization to retrofit.
     */
    public GsonConverterFactory gsonConverterFactory;

    /**
     * RxJava2 call adapter factory to be used to
     * handle observables.
     */
    public RxJava2CallAdapterFactory rxJava2CallAdapterFactory;

    /**
     * Creates a new Retrofit Helper.
     *
     * @param baseUrl The base URL for all API calls that will be retrofitted.
     * @param gsonConverterFactory The GSON converter factory to be used.
     * @param rxJava2CallAdapterFactory The RxJava2 call adapter factory to be used.
     */
    public RetrofitHelper(String baseUrl, GsonConverterFactory gsonConverterFactory,
                          RxJava2CallAdapterFactory rxJava2CallAdapterFactory)
    {
        this.baseUrl = baseUrl;
        this.gsonConverterFactory = gsonConverterFactory;
        this.rxJava2CallAdapterFactory = rxJava2CallAdapterFactory;
    }

    /**
     * Provides the retrofit instance.
     *
     * @return a new retrofit instance.
     */
    public Retrofit provideRetrofit()
    {
        return new Retrofit.Builder()
                .baseUrl(baseUrl).addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(rxJava2CallAdapterFactory)
                .build();
    }
}
