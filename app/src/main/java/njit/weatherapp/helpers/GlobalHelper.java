package njit.weatherapp.helpers;

import android.Manifest;

/**
 * Helps provide global values to be injected into other services.
 */
public class GlobalHelper
{
    /** Base URL of the Weather API endpoint. **/
    public static final String WEATHER_BASE_URL = "https://api.openweathermap.org/data/2.5/";

    /** Base URL of the Maps API endpoint. **/
    public static final String MAPS_BASE_URL = "https://maps.googleapis.com/";

    /** Authentication key needed for API authorization. **/
    public static final String WEATHER_API_KEY = "d1c97b171b6602e80f22198235e78a94";

    /** Authentication key needed for API authorization. **/
    public static final String MAPS_API_KEY = "AIzaSyCqhktcT00PUXXanCsuJvKqCFbkPFSGNWA";

    /** Location updates interval - 10sec **/
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    public static final int REQUEST_CHECK_SETTINGS = 100;

    /** Required permissions for first time app startup. **/
    public static final String[] permissions = {
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.INTERNET
    };

}
