package njit.weatherapp.helpers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Helps provide GSON to be injected into other services.
 */
public class GsonHelper
{
    /**
     * Provides a new GSON instance.
     *
     * @return a new GSON instance.
     */
    private static Gson provideGsonBuilder()
    {
        return new GsonBuilder().create();
    }

    /**
     * Provides a new GSON converter factory.
     *
     * @return a new GSON converter factory.
     */
    public static GsonConverterFactory provideGsonConverterFactory()
    {
        return GsonConverterFactory.create(provideGsonBuilder());
    }
}
