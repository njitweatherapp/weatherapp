package njit.weatherapp.injection;

import njit.weatherapp.contracts.IMapsApiHandler;
import njit.weatherapp.contracts.IMetricConverter;
import njit.weatherapp.contracts.IWeatherApiHandler;
import njit.weatherapp.helpers.ApiHelper;
import njit.weatherapp.helpers.GsonHelper;
import njit.weatherapp.helpers.RetrofitHelper;
import njit.weatherapp.helpers.RxJavaHelper;
import njit.weatherapp.libraries.ForecastCalculator;
import njit.weatherapp.libraries.MetricConverter;

/**
 * Helps inject dependencies into the application.
 */
public class Injector
{
    /**
     * Injects a new Retrofit helper class.
     *
     * @param baseUrl The base URL of the API being retrofitted.
     * @return a new Retrofit helper.
     */
    public static RetrofitHelper injectRetroFit(String baseUrl)
    {
        return new RetrofitHelper(baseUrl,
                GsonHelper.provideGsonConverterFactory(),
                RxJavaHelper.provideRxJava2CallAdapterFactory());
    }

    /**
     * Injects a new API helper class.
     *
     * @param retrofitHelper The retrofit helper dependency..
     * @return A new API helper class.
     */
    public static ApiHelper injectApiHelper(RetrofitHelper retrofitHelper)
    {
        return new ApiHelper(retrofitHelper.provideRetrofit());
    }

    /**
     * Injects a new Weather API handler class.
     *
     * @param apiHelper The API helper dependency.
     *
     * @return A new API helper class.
     */
    public static IWeatherApiHandler injectWeatherApiHandler(ApiHelper apiHelper)
    {
        return apiHelper.provideWeatherApiHandler();
    }

    /**
     * Injects a new Maps API handler class.
     *
     * @param apiHelper The Api helper dependancy.
     * @return A new API helper class.
     */
    public static IMapsApiHandler injectMapsApiHandler(ApiHelper apiHelper)
    {
        return apiHelper.provideMapsApiHandler();
    }

    /**
     * Injects a new metric converter library.
     *
     * @return The metric converter library.
     */
    public static MetricConverter injectMetricConverter()
    {
        return new MetricConverter();
    }

    /**
     * Injects a new forecast calculator.
     *
     * @param metricConverter The metric converter library.
     * @return A new forecast calculator class.
     */
    public static ForecastCalculator injectForecastCalculator(IMetricConverter metricConverter)
    {
        return new ForecastCalculator(metricConverter);
    }
}
