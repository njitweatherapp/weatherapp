package njit.weatherapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class City
{
    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("coord")
    @Expose
    private Coordinates coordinates;

    @SerializedName("country")
    @Expose
    private String country;

    @SerializedName("cod")
    @Expose
    private String cod;

    @SerializedName("message")
    @Expose
    private Double message;

    @SerializedName("cnt")
    @Expose
    private Integer cnt;

    @SerializedName("list")
    @Expose
    private List<WeatherDataList> weatherDatalist = null;

    /**
     * Gets the city id.
     *
     * @return The city id.
     */
    public Integer getId()
    {
        return id;
    }

    /**
     * Sets the city id.
     *
     * @param id The city id.
     */
    public void setId(Integer id)
    {
        this.id = id;
    }

    /**
     * Gets the city name.
     *
     * @return The name of the city.
     */
    public String getName()
    {
        return name;
    }

    /**
     * Sets the city name.
     *
     * @param name The name of the city.
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Gets the city coordinates.
     *
     * @return The coordinates of the city.
     */
    public Coordinates getCoordinates()
    {
        return coordinates;
    }

    /**
     * Sets the city coordinates.
     *
     * @param coordinates The coordinates of the city.
     */
    public void setCoordinates(Coordinates coordinates)
    {
        this.coordinates = coordinates;
    }

    /**
     * Gets the country of the city.
     *
     * @return The country of the city.
     */
    public String getCountry()
    {
        return country;
    }

    /**
     * Sets the country of the city.
     *
     * @param country The country of the city.
     */
    public void setCountry(String country)
    {
        this.country = country;
    }

    /**
     * Gets the internal parameter.
     *
     * @return An internal parameter.
     * @@since This is an internal parameter for use outside of this application.
     *         Do not change it's value!
     */
    public String getCod()
    {
        return cod;
    }

    /**
     * Sets the internal parameter.
     *
     * @param cod The internal parameter.
     * @@since This is an internal parameter for use outside of this application.
     *         Do not change it's value!
     */
    public void setCod(String cod)
    {
        this.cod = cod;
    }

    /**
     * Gets the internal parameter message.
     *
     * @return The internal paramater message.
     * @@since This is an internal parameter for use outside of this application.
     *         Do not change it's value!
     */
    public Double getMessage()
    {
        return message;
    }

    /**
     * Sets the internal parameter message.
     *
     * @param message The internal parameter message.
     * @@since This is an internal parameter for use outside of this application.
     *         Do not change it's value!
     */
    public void setMessage(Double message)
    {
        this.message = message;
    }

    /**
     * Gets the number of lines returned from the API call.
     *
     * @return The number of lines returned from the API call.
     */
    public Integer getCnt()
    {
        return cnt;
    }

    /**
     * Sets the number of lines returned from the API call.
     *
     * @param cnt The number of lines returned from the API call.
     */
    public void setCnt(Integer cnt)
    {
        this.cnt = cnt;
    }

    /**
     * Gets the list of weather data.
     *
     * @return The list of weather data.
     */
    public List<WeatherDataList> getWeatherDatalistList()
    {
        return weatherDatalist;
    }

    /**
     * Sets the list of weather data.
     *
     * @param weatherDatalist The weather data list.
     */
    public void setWeatherDatalistList(List<WeatherDataList> weatherDatalist)
    {
        this.weatherDatalist = weatherDatalist;
    }
}
