package njit.weatherapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Holds the main weather data for a location.
 */
public class Main
{
    @SerializedName("temp")
    @Expose
    private Double temp;

    @SerializedName("humidity")
    @Expose
    private Integer humidity;

    @SerializedName("pressure")
    @Expose
    private Double pressure;

    @SerializedName("temp_min")
    @Expose
    private Double tempMin;

    @SerializedName("temp_max")
    @Expose
    private Double tempMax;

    @SerializedName("sea_level")
    @Expose
    private Double seaLevel;

    @SerializedName("grnd_level")
    @Expose
    private Double grndLevel;

    @SerializedName("temp_kf")
    @Expose
    private Double tempKf;

    /**
     * Gets the weather temperature.
     *
     * @return The temperature.
     */
    public Double getTemp()
    {
        return temp;
    }

    /**
     * Sets the weather temperature.
     *
     * @param temp The temperature.
     */
    public void setTemp(Double temp)
    {
        this.temp = temp;
    }

    /**
     * Gets the outside humidity.
     *
     * @return The weather humidity.
     */
    public Integer getHumidity()
    {
        return humidity;
    }

    /**
     * Sets the outside humidity.
     *
     * @param humidity The outside humidity.
     */
    public void setHumidity(Integer humidity)
    {
        this.humidity = humidity;
    }

    /**
     * Gets the outside pressure.
     *
     * @return The outside pressure.
     */
    public Double getPressure()
    {
        return pressure;
    }

    /**
     * Sets the outside pressure.
     *
     * @param pressure The outside pressure.
     */
    public void setPressure(Double pressure)
    {
        this.pressure = pressure;
    }

    /**
     * Gets the minimum outside temperature.
     *
     * @return The minimum temperature.
     */
    public Double getTempMin()
    {
        return tempMin;
    }

    /**
     * Sets the minimum outside temperature.
     *
     * @param tempMin The minimum outside temperature.
     */
    public void setTempMin(Double tempMin)
    {
        this.tempMin = tempMin;
    }

    /**
     * Gets the max outside temperature.
     *
     * @return The max outside temperature.
     */
    public Double getTempMax()
    {
        return tempMax;
    }

    /**
     * Sets the max outside temperature.
     *
     * @param tempMax The max outside temperature
     */
    public void setTempMax(Double tempMax)
    {
        this.tempMax = tempMax;
    }

    /**
     * Gets the sea level of the weather location in hPa measurement.
     *
     * @return The sea level in hPa measurement.
     */
    public Double getSeaLevel()
    {
        return seaLevel;
    }

    /**
     * Sets the sea level of the weather location in hPa.
     *
     * @param seaLevel The sea level in hPa.
     */
    public void setSeaLevel(Double seaLevel)
    {
        this.seaLevel = seaLevel;
    }

    /**
     * Gets the weather location ground level in hPa measurement.
     *
     * @return The ground level in hPa measurement.
     */
    public Double getGrndLevel()
    {
        return grndLevel;
    }


    /**
     * Sets the weather location ground level in hPa.
     *
     * @param grndLevel The ground level in hPa.
     */
    public void setGrndLevel(Double grndLevel)
    {
        this.grndLevel = grndLevel;
    }

    /**
     * Gets the internal parameter.
     *
     * @return Internal parameter.
     * @@since This is an internal parameter for use outside of this application.
     *         Do not change it's value!
     */
    public Double getTempKf()
    {
        return tempKf;
    }

    /**
     * Sets the internal parameter.
     *
     * @param tempKf Internal parameter.
     * @@since This is an internal parameter for use outside of this application.
     *         Do not change it's value!
     */
    public void setTempKf(Double tempKf)
    {
        this.tempKf = tempKf;
    }
}
