package njit.weatherapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Holds data for amount of cloud coverage.
 */
public class Clouds
{
    @SerializedName("all")
    @Expose
    private Integer all;

    /**
     * Gets probability of cloud coverage.
     *
     * @return returns probability fo cloud coverage.
     */
    public Integer getAll()
    {
        return all;
    }

    /**
     * Sets probability of cloud coverage.
     */
    public void setAll(Integer all)
    {
        this.all = all;
    }
}
