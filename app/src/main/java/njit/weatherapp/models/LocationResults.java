package njit.weatherapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * The location data result returned from the Google Maps API.
 */
public class LocationResults
{
    @SerializedName("results")
    @Expose
    private List<GeoData> results;

    @SerializedName("status")
    @Expose
    private String status;

    /**
     * Gets the results from Google Maps.
     *
     * @return Geo data from Google maps.
     */
    public List<GeoData> getResults()
    {
        return results;
    }

    /**
     * Sets the results from Google Maps.
     *
     * @param results The geo data.
     */
    public void setResults(List<GeoData> results)
    {
        this.results = results;
    }

    /**
     * Gets the REST response.
     *
     * @return The REST response.
     */
    public String getStatus()
    {
        return status;
    }

    /**
     * Sets the REST response.
     *
     * @param status The REST response.
     */
    public void setStatus(String status)
    {
        this.status = status;
    }
}
