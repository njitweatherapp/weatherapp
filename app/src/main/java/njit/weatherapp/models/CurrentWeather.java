package njit.weatherapp.models;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Holds data about the current weather.
 */
public class CurrentWeather
{
    @SerializedName("coord")
    @Expose
    private Coordinates coordinates;

    @SerializedName("sys")
    @Expose
    private Sys sys;

    @SerializedName("weather")
    @Expose
    private List<Weather> weather = null;

    @SerializedName("main")
    @Expose
    private Main main;

    @SerializedName("wind")
    @Expose
    private Wind wind;

    @SerializedName("rain")
    @Expose
    private Rain rain;

    @SerializedName("clouds")
    @Expose
    private Clouds clouds;

    @SerializedName("dt")
    @Expose
    private Integer dt;

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("cod")
    @Expose
    private Integer cod;

    /**
     * Gets the weather location coordinates.
     *
     * @return The weather location coordinates.
     */
    public Coordinates getCoordinates()
    {
        return coordinates;
    }

    /**
     * Sets the weather location coordinates.
     *
     * @param coordinates The weather location coordinates.
     */
    public void setCoord(Coordinates coordinates)
    {
        this.coordinates = coordinates;
    }

    /**
     * Gets the meta weather data.
     *
     * @return Meta weather data.
     */
    public Sys getSys()
    {
        return sys;
    }

    /**
     * Sets the meta weather data.
     *
     * @param sys The meta weather data.
     */
    public void setSys(Sys sys)
    {
        this.sys = sys;
    }

    /**
     * Gets the weather data.
     *
     * @return A list of weather data.
     */
    public List<Weather> getWeather()
    {
        return weather;
    }

    /**
     * Set the weather data.
     *
     * @param weather The weather data.
     */
    public void setWeather(List<Weather> weather)
    {
        this.weather = weather;
    }

    /**
     * Gets main weather data.
     *
     * @return Main weather data.
     */
    public Main getMain()
    {
        return main;
    }

    /**
     * Sets the main weather data.
     *
     * @param main The main weather data.
     */
    public void setMain(Main main)
    {
        this.main = main;
    }

    /**
     * Gets the wind weather data.
     *
     * @return The wind weather data.
     */
    public Wind getWind()
    {
        return wind;
    }

    /**
     * Sets the wind weather data.
     *
     * @param wind The wind weather data.
     */
    public void setWind(Wind wind)
    {
        this.wind = wind;
    }

    /**
     * Gets the rain weather data.
     *
     * @return Rain weather data.
     */
    public Rain getRain()
    {
        return rain;
    }

    /**
     * Sets the rain weather data.
     *
     * @param rain Rain weather data.
     */
    public void setRain(Rain rain)
    {
        this.rain = rain;
    }

    /**
     * Gets cloud weather data.
     *
     * @return Cloud weather data.
     */
    public Clouds getClouds()
    {
        return clouds;
    }

    /**
     * Sets the cloud weather data.
     *
     * @param clouds Cloud weather data.
     */
    public void setClouds(Clouds clouds) {
        this.clouds = clouds;
    }

    /**
     * Gets the time of data calculation in UTC.
     *
     * @return Time of data calculation in UTC.
     */
    public Integer getDt()
    {
        return dt;
    }

    /**
     * Sets the time of data calculation in UTC.
     *
     * @param dt Time of data calculation in UTC.
     */
    public void setDt(Integer dt)
    {
        this.dt = dt;
    }

    /**
     * Gets the city id for the weather data.
     *
     * @return The id of the city.
     */
    public Integer getId()
    {
        return id;
    }

    /**
     * Sets the city id for the weather data.
     *
     * @param id The id of the city.
     */
    public void setId(Integer id)
    {
        this.id = id;
    }

    /**
     * Gets the name of the city for the weather data.
     *
     * @return The name of the city for the weather data.
     */
    public String getName()
    {
        return name;
    }

    /**
     * Sets the name of the city for the weather data.
     *
     * @param name
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Gets the internal API parameter.
     *
     * @return Internal API parameter.
     * @since This is an internal parameter for use outside of this application.
     *        Do not change it's value!
     */
    public Integer getCod()
    {
        return cod;
    }

    /**
     * Sets the internal API parameter.
     *
     * @param cod Internal API parameter.
     * @since This is an internal parameter for use outside of this application.
     *        Do not change it's value!
     */
    public void setCod(Integer cod)
    {
        this.cod = cod;
    }
}
