package njit.weatherapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Holds other meta weather data.
 */
public class Sys
{
    @SerializedName("country")
    @Expose
    private String country;

    @SerializedName("sunrise")
    @Expose
    private Integer sunrise;

    @SerializedName("sunset")
    @Expose
    private Integer sunset;

    @SerializedName("pod")
    @Expose
    private String pod;

    /**
     * Gets the country of the weather data.
     *
     * @return The country.
     */
    public String getCountry()
    {
        return country;
    }

    /**
     * Sets the country of the weather data.
     *
     * @param country The country.
     */
    public void setCountry(String country)
    {
        this.country = country;
    }

    /**
     * Gets the time of sunrise in UTC.
     *
     * @return The time of sunrise.
     */
    public Integer getSunrise()
    {
        return sunrise;
    }

    /**
     * Sets the time of sunrise in UTC.
     *
     * @param sunrise The time of sunrise.
     */
    public void setSunrise(Integer sunrise)
    {
        this.sunrise = sunrise;
    }

    /**
     * Gets the time of sunset in UTC.
     *
     * @return Time of sunset in UTC.
     */
    public Integer getSunset()
    {
        return sunset;
    }

    /**
     * Sets the time of sunset in UTC.
     *
     * @param sunset
     */
    public void setSunset(Integer sunset)
    {
        this.sunset = sunset;
    }

    /**
     * Gets internal parameter.
     *
     * @return Internal parameter.
     * @since This is an internal parameter for use outside of this application.
     *         Do not change it's value!
     */
    public String getPod()
    {
        return pod;
    }

    /**
     * Sets internal parameter.
     *
     * @param pod Internal parameter.
     * @@since This is an internal parameter for use outside of this application.
     *         Do not change it's value!
     */
    public void setPod(String pod)
    {
        this.pod = pod;
    }
}
