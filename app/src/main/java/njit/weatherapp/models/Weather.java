package njit.weatherapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Holds the weather data.
 */
public class Weather
{

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("main")
    @Expose
    private String main;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("icon")
    @Expose
    private String icon;

    /**
     * Gets the weather icon id.
     *
     * @return The weather icon id.
     */
    public Integer getId()
    {
        return id;
    }

    /**
     * Sets the weather icon id.
     * @param id The weather icon id.
     */
    public void setId(Integer id)
    {
        this.id = id;
    }

    /**
     * Gets the main weather data.
     *
     * @return The main weather data.
     */
    public String getMain()
    {
        return main;
    }

    /**
     * Sets the main weather data.
     *
     * @param main The main weather data.
     */
    public void setMain(String main)
    {
        this.main = main;
    }

    /**
     * Gets the weather description.
     *
     * @return The weather description.
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Sets the weather description.
     *
     * @param description The weather description.
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * Gets the weather icon.
     *
     * @return The weather icon.
     */
    public String getIcon()
    {
        return icon;
    }

    /**
     * Sets the weather icon.
     *
     * @param icon The weather icon.
     */
    public void setIcon(String icon)
    {
        this.icon = icon;
    }
}
