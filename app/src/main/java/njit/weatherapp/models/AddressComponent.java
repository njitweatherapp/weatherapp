package njit.weatherapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Holds the separate components of an address.
 */
public class AddressComponent
{
    @SerializedName("long_name")
    @Expose
    private String longName;

    @SerializedName("short_name")
    @Expose
    private String shortName;

    @SerializedName("types")
    @Expose
    private List<String> types;

    /**
     * Gets the long name of the address.
     *
     * @return The long name of the address.
     */
    public String getLongName()
    {
        return longName;
    }

    /**
     * Sets the long name of the address.
     *
     * @param longName The long name of the address.
     */
    public void setLongName(String longName)
    {
        this.longName = longName;
    }

    /**
     * Gets the short name of the address.
     *
     * @return The short name of the address.
     */
    public String getShortName()
    {
        return shortName;
    }

    /**
     * Sets the short name of the address.
     *
     * @param shortName The short name of the address.
     */
    public void setShortName(String shortName)
    {
        this.shortName = shortName;
    }

    /**
     * Gets the address types.
     *
     * @return The address types.
     */
    public List<String> getTypes()
    {
        return types;
    }

    /**
     * Sets the address types.
     *
     * @param types The address types.
     */
    public void setTypes(List<String> types)
    {
        this.types = types;
    }
}
