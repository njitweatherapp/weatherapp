package njit.weatherapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Holds data about the wind conditions.
 */
public class Wind
{
    @SerializedName("speed")
    @Expose
    private Double speed;

    @SerializedName("deg")
    @Expose
    private Double deg;

    /**
     * Gets the wind speed in meter/sec.
     *
     * @return The wind speed.
     */
    public Double getSpeed()
    {
        return speed;
    }

    /**
     * Sets the wind speed in meter/sec.
     *
     * @param speed The wind speed in meter/sec.
     */
    public void setSpeed(Double speed)
    {
        this.speed = speed;
    }

    /**
     * Wind direction in meteorological degrees.
     *
     * @return The wind direction.
     */
    public Double getDeg()
    {
        return deg;
    }

    /**
     * Sets the wind direction in meteorological degrees.
     *
     * @param deg The wind direction in degrees.
     */
    public void setDeg(Double deg)
    {
        this.deg = deg;
    }
}
