package njit.weatherapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Location coordinates of weather data.
 */
public class Coordinates
{

    @SerializedName("lon")
    @Expose
    private double longitude;

    @SerializedName("lat")
    @Expose
    private double latitude;

    /**
     * Gets longitude of coordinates.
     *
     * @return The longitude of the coordinate set.
     */
    public double getLongitude()
    {
        return longitude;
    }

    /**
     * Sets the longitude coordinate.
     *
     * @param longitude the coordinate value for longitude.
     */
    public void setLongitude(double longitude)
    {
        this.longitude = longitude;
    }

    /**
     * Gets the latitude coordinate.
     *
     * @return The latitude of the coordinate set.
     */
    public double getLatitude()
    {
        return latitude;
    }

    /**
     * Sets the latitude coordinate.
     *
     * @param latitude
     */
    public void setLatitude(double latitude)
    {
        this.latitude = latitude;
    }
}
