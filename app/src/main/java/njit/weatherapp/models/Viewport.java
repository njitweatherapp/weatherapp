package njit.weatherapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Holds the viewport directional coordinates.
 */
public class Viewport
{
    @SerializedName("northeast")
    @Expose
    private Location northeast;

    @SerializedName("southwest")
    @Expose
    private Location southwest;


    /**
     * Gets the northeast viewport coordinates.
     *
     * @return The northeast viewport coordinates.
     */
    public Location getNortheast()
    {
        return northeast;
    }

    /**
     * Sets the northeast viewport coordinates.
     *
     * @param northeast
     */
    public void setNortheast(Location northeast)
    {
        this.northeast = northeast;
    }

    /**
     * Gets the southwest viewport coordinate.
     *
     * @return The southwest viewport coordinate.
     */
    public Location getSouthwest()
    {
        return southwest;
    }

    /**
     * Sets the southwest viewport coordinates.
     *
     * @param southwest The southwest viewport coordinates.
     */
    public void setSouthwest(Location southwest)
    {
        this.southwest = southwest;
    }
}
