package njit.weatherapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Holds data for the probability of rain.
 */
public class Rain
{
    @SerializedName("3h")
    @Expose
    private Double rainVolume;

    /**
     * Gets the rain volume for a location.
     * @return
     */
    public Double getRainVolume()
    {
        return rainVolume;
    }

    /**
     * Sets the rain volume for a location.
     * @param rainVolume
     */
    public void setRainVolume(Double rainVolume)
    {
        this.rainVolume = rainVolume;
    }
}
