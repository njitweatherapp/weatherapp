package njit.weatherapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Holds the forecast data.
 */
public class Forecast
{
    @SerializedName("city")
    @Expose
    private City city;

    /**
     * Gets the city.
     *
     * @return The city.
     */
    public City getCity()
    {
        return city;
    }

    /**
     * Sets the city.
     *
     * @param city The city.
     */
    public void setCity(City city)
    {
        this.city = city;
    }

}
