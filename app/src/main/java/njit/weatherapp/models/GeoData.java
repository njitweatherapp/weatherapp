package njit.weatherapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Holds Google maps geo data.
 */
public class GeoData
{
    @SerializedName("address_components")
    @Expose
    private List<AddressComponent> addressComponents;

    @SerializedName("formatted_address")
    @Expose
    private String formattedAddress;

    @SerializedName("geometry")
    @Expose
    private Geometry geometry;

    @SerializedName("place_id")
    @Expose
    private String placeID;

    @SerializedName("types")
    @Expose
    private List<String> types;

    /**
     * Gets the address components.
     *
     * @return The address components.
     */
    public List<AddressComponent> getAddressComponents()
    {
        return addressComponents;
    }

    /**
     * Sets the address components.
     *
     * @param addressComponents The address components.
     */
    public void setAddressComponents(List<AddressComponent> addressComponents)
    {
        this.addressComponents = addressComponents;
    }

    /**
     * Gets the formatted address.
     *
     * @return The formatted address.
     */
    public String getFormattedAddress()
    {
        return formattedAddress;
    }

    /**
     * Sets the formatted address.
     *
     * @param formattedAddress The formatted address.
     */
    public void setFormattedAddress(String formattedAddress)
    {
        this.formattedAddress = formattedAddress;
    }

    /**
     * Gets the geometry (coordinates).
     *
     * @return The coordinate data.
     */
    public Geometry getGeometry()
    {
        return geometry;
    }

    /**
     * Sets the geometry (coordinates).
     *
     * @param geometry The coordinate data.
     */
    public void setGeometry(Geometry geometry)
    {
        this.geometry = geometry;
    }

    /**
     * Gets the place id for the Places API.
     *
     * @return The place id for the Places API/
     */
    public String getPlaceID()
    {
        return placeID;
    }

    /**
     * Sets the place id for the Places API.
     *
     * @param placeID The id for the Places API.
     */
    public void setPlaceID(String placeID)
    {
        this.placeID = placeID;
    }

    /**
     * Gets the location types.
     *
     * @return The location types.
     */
    public List<String> getTypes()
    {
        return types;
    }

    /**
     * Sets the location types.
     *
     * @param types The location types.
     */
    public void setTypes(List<String> types)
    {
        this.types = types;
    }
}
