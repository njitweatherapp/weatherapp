package njit.weatherapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Holds the forecast results from the Open Weather API.
 */
public class ForecastResults
{
    @SerializedName("cod")
    @Expose
    private String cod;

    @SerializedName("message")
    @Expose
    private double message;

    @SerializedName("cnt")
    @Expose
    private long cnt;

    @SerializedName("list")
    @Expose
    List<WeatherDataList> weatherDataList;

    @SerializedName("city")
    @Expose
    private City city;

    /**
     * Gets the REST API response code.
     *
     * @return The REST API response code.
     */
    public String getCod()
    {
        return cod;
    }

    /**
     * Sets the REST API code.
     *
     * @param cod The REST API response code.
     */
    public void setCod(String cod)
    {
        this.cod = cod;
    }

    /**
     * Gets internal parameter.
     *
     * @return Internal parameter.
     * @since This is an internal parameter for use outside of this application.
     *         Do not change it's value!
     */
    public double getMessage()
    {
        return message;
    }

    /**
     * Sets internal parameter.
     *
     * @return Internal parameter.
     * @since This is an internal parameter for use outside of this application.
     *         Do not change it's value!
     */
    public void setMessage(double message)
    {
        this.message = message;
    }

    /**
     * Gets internal parameter.
     *
     * @return Internal parameter.
     * @since This is an internal parameter for use outside of this application.
     *         Do not change it's value!
     */
    public long getCnt()
    {
        return cnt;
    }

    /**
     * Sets internal parameter.
     *
     * @return Internal parameter.
     * @since This is an internal parameter for use outside of this application.
     *         Do not change it's value!
     */
    public void setCnt(long cnt)
    {
        this.cnt = cnt;
    }

    /**
     * Gets the weather data.
     *
     * @return The weather data.
     */
    public List<WeatherDataList> getWeatherDataList()
    {
        return weatherDataList;
    }

    /**
     * Sets the weather data.
     *
     * @param weatherDataList The weather data.
     */
    public void setWeatherDataList(List<WeatherDataList> weatherDataList)
    {
        this.weatherDataList = weatherDataList;
    }

    /**
     * Gets the city of the forecast.
     *
     * @return The city of the forecast.
     */
    public City getCity()
    {
        return city;
    }

    /**
     * Sets the city of the forecast.
     *
     * @param city The city of the forecast.
     */
    public void setCity(City city)
    {
        this.city = city;
    }
}
