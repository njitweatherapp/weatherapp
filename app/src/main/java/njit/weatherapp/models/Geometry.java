package njit.weatherapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Holds the geometry coordinate data.
 */
public class Geometry
{
    @SerializedName("location")
    @Expose
    private Location location;

    @SerializedName("location_type")
    @Expose
    private String locationType;

    @SerializedName("viewport")
    @Expose
    private Viewport viewport;

    /**
     * Gets the location data.
     *
     * @return The location data.
     */
    public Location getLocation()
    {
        return location;
    }

    /**
     * Sets the location data.
     *
     * @param location The location data.
     */
    public void setLocation(Location location)
    {
        this.location = location;
    }

    /**
     * Gets the location type.
     *
     * @return The location type.
     */
    public String getLocationType()
    {
        return locationType;
    }

    /**
     * Sets the location type.
     *
     * @param locationType The location type.
     */
    public void setLocationType(String locationType)
    {
        this.locationType = locationType;
    }

    /**
     * Gets the viewport coordinates.
     *
     * @return The viewport coordinates.
     */
    public Viewport getViewport()
    {
        return viewport;
    }

    /**
     * Sets the viewport coordinates.
     *
     * @param viewport The viewport coordinates.
     */
    public void setViewport(Viewport viewport)
    {
        this.viewport = viewport;
    }
}
