package njit.weatherapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Holds the location coordinates.
 */
public class Location
{
    @SerializedName("lat")
    @Expose
    private double latitude;

    @SerializedName("lng")
    @Expose
    private double longitude;

    /**
     * Gets the latitude of the location coordinate.
     *
     * @return The latitude of the location coordinate.
     */
    public double getLatitude()
    {
        return latitude;
    }

    /**
     * Sets the latitude of the location coordinate.
     *
     * @param latitude The latitude of the location coordinate.
     */
    public void setLatitude(double latitude)
    {
        this.latitude = latitude;
    }

    /**
     * Gets the longitude of the location coordinate.
     *
     * @return The longitude of the location coordinate.
     */
    public double getLongitude()
    {
        return longitude;
    }


    /**
     * Sets the longitude of the location coordinate.
     *
     * @param longitude The longitude of a location coordinate.
     */
    public void setLng(double longitude)
    {
        this.longitude = longitude;
    }
}
