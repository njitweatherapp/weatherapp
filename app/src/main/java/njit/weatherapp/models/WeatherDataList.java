package njit.weatherapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WeatherDataList
{
    @SerializedName("dt")
    @Expose
    private Integer dt;

    @SerializedName("main")
    @Expose
    private Main main;

    @SerializedName("weather")
    @Expose
    private List<Weather> weather = null;

    @SerializedName("clouds")
    @Expose
    private Clouds clouds;

    @SerializedName("wind")
    @Expose
    private Wind wind;

    @SerializedName("rain")
    @Expose
    private Rain rain;

    @SerializedName("sys")
    @Expose
    private Sys sys;

    @SerializedName("dt_txt")
    @Expose
    private String dtTxt;

    /**
     * Gets the time of data forecasted in UTC.
     *
     * @return The date time of forecast in UTC.
     */
    public Integer getDt()
    {
        return dt;
    }

    /**
     * Sets the time of data forecasted in UTC.
     *
     * @param dt The date time of forecast in UTC.
     */
    public void setDt(Integer dt)
    {
        this.dt = dt;
    }

    /**
     * Gets the main weather data.
     *
     * @return The main weather data.
     */
    public Main getMain()
    {
        return main;
    }

    /**
     * Sets the main weather data.
     *
     * @param main The main weather data.
     */
    public void setMain(Main main)
    {
        this.main = main;
    }

    /**
     * Gets the weather data.
     *
     * @return A list of weather data.
     */
    public List<Weather> getWeather()
    {
        return weather;
    }

    /**
     * Sets the weather data.
     *
     * @param weather List of weather data.
     */
    public void setWeather(java.util.List<Weather> weather)
    {
        this.weather = weather;
    }

    /**
     * Gets the cloud weather data.
     *
     * @return The cloud weather data.
     */
    public Clouds getClouds()
    {
        return clouds;
    }

    /**
     * Sets the cloud weather data.
     *
     * @param clouds The cloud weather data.
     */
    public void setClouds(Clouds clouds)
    {
        this.clouds = clouds;
    }

    /**
     * Gets the wind weather data.
     *
     * @return The wind weather data.
     */
    public Wind getWind()
    {
        return wind;
    }

    /**
     * Sets the wind weather data.
     *
     * @param wind The wind weather data.
     */
    public void setWind(Wind wind)
    {
        this.wind = wind;
    }

    /**
     * Gets the rain data.
     *
     * @return The rain data.
     */
    public Rain getRain()
    {
        return rain;
    }

    /**
     * Sets the rain data.
     *
     * @param rain The rain data.
     */
    public void setRain(Rain rain)
    {
        this.rain = rain;
    }

    /**
     * Gets the weather meta data.
     *
     * @return The weather meta data.
     */
    public Sys getSys()
    {
        return sys;
    }

    /**
     * Sets the weather meta data.
     *
     * @param sys The weather meta data.
     */
    public void setSys(Sys sys)
    {
        this.sys = sys;
    }

    /**
     * Gets the tine of calculation in UTC string format.
     *
     * @return Time of calculation in UTC string format.
     */
    public String getDtTxt()
    {
        return dtTxt;
    }

    /**
     * Sets the time of calculation in UTC string format.
     *
     * @param dtTxt Time of calculation in UTC string format.
     */
    public void setDtTxt(String dtTxt)
    {
        this.dtTxt = dtTxt;
    }
}
